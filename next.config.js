/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["www.mercedes-benz.com"],
  },
}

module.exports = nextConfig

