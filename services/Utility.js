export function formatMoney(value, type = "VND") {
  if (typeof value !== "number") {
    return value;
  }
  var formatter;
  // return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  if (type == "VND") {
    formatter = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
      minimumFractionDigits: 0,
    });
    var new_value = formatter.format(value);
    return new_value
      .replace(/,/g, "_")
      .replace(/\./g, ".")
      .replace(/_/g, ".")
      .replace("₫", "");
  }

  if (type == "USD") {
    formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 2,
    });
    return formatter.format(value);
  }

  if (type == "EUR") {
    formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "EUR",
      minimumFractionDigits: 2,
    });
    return formatter.format(value);
  }
}
