import React, { memo, useState } from "react";
// import { withTranslation } from '../i18n';

export const NumberFormatPrice = (number) => {
    if (number) {
        var numbernew = number.slice(-1) === '₫' ? number.slice(0, -1) : number;
        return new Intl.NumberFormat().format(numbernew);
    }
    return 0;
}
export const getSale = (reule, sale) => {
    if (reule && sale) {
        var reulenew = reule.slice(-1) === '₫' ? reule.slice(0, -1) : reule;
        var salenew = sale.slice(-1) === '₫' ? sale.slice(0, -1) : sale;
        return Math.round(((reulenew - salenew) / reulenew) * 100);
    }
    return 0;
}

export const getCountRating = (dataArr, sao) => {
    var count = 0
    dataArr.forEach(element => {
        if (sao === element.rating) {
            count++
        }
    });
    return count;
}
export const getTileCountRating = (data_arr) => {
    let ojectRating = {
        sao5: getCountRating(data_arr, 5),
        sao4: getCountRating(data_arr, 4),
        sao3: getCountRating(data_arr, 3),
        sao2: getCountRating(data_arr, 2),
        sao1: getCountRating(data_arr, 1)
    }
    var count = 1
    var name = 'sao5'
    //tim ti le cao nhat
    Object.entries(ojectRating).forEach(element => {
        if (count <= element[1]) {
            name = element[0];
            count = element[1];
        }
    });
    //set ti le 
    var ojbet = []
    Object.entries(ojectRating).forEach(element => {
        ojbet[element[0]] = Math.round(element[1] / count * 10)
    });
    return Object.assign({}, ojbet);
}
export const getDateNho = (timeinput, lang = 'vi') => {
    var dateNew = new Date(timeinput);
    let minN = dateNew.getMinutes();
    let hourN = dateNew.getHours() + 7;
    let dateN = dateNew.getDate();
    let monthN = dateNew.getMonth();
    let yearN = dateNew.getFullYear();


    let newDate = new Date()
    let min = newDate.getMinutes();
    let hour = newDate.getHours();
    let date = newDate.getDate();
    let month = newDate.getMonth();
    let year = newDate.getFullYear();

    if (yearN < year) {
        return year - yearN + (lang == 'vi' ? ' năm trước' : ' year ago')
    }
    if (monthN < month) {
        return month - monthN + (lang == 'vi' ? ' tháng trước' : ' month ago')
    }
    if (dateN < date) {
        return date - dateN + (lang == 'vi' ? ' ngày trước' : ' day ago')
    }
    if (hourN < hour) {
        return hour - hourN + (lang == 'vi' ? ' giờ trước' : ' hour ago')
    }
    return min - minN + (lang == 'vi' ? ' phút trước' : ' minute ago')
}
export const formatDate = (date) => {
    if (!date) {
        return "01.01.2021";
    }
    const d2 = new Date(date);
    const ye2 = new Intl.DateTimeFormat('en', { year: 'numeric' })?.format(d2);
    const mo2 = new Intl.DateTimeFormat('en', { month: 'numeric' })?.format(d2);
    const da2 = new Intl.DateTimeFormat('en', { day: '2-digit' })?.format(d2);
    return `${da2.length == 1 ? '0' + da2 : da2}.${mo2.length == 1 ? '0' + mo2 : mo2}.${ye2}`;
}