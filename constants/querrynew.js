import axios from "axios";

// khởi tạo khung grap
const useGrapql = (query, token = "", session = "") => {
  let headers = {
    // 'Cache-Control': 'no-cache'
  };
  if (token !== "") {
    headers = {
      ...headers,
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
      Accept: "application/json",
    };
  }
  if (session !== "") {
    headers = {
      ...headers,
      "woocommerce-session": `Session ${session}`,
    };
  }
  return axios({
    method: "POST",
    url: `${process.env.NEXT_PUBLIC_HOSTNAMEA}`,
    data: {
      query: query,
    },
    headers: headers,
    timeout: 90000,
  }).catch((e) => console.log("e", process.env.HOSTNAME));
};

const useRefullAPI = ( query, token = "", session = "") =>{
  let headers = {
    // 'Cache-Control': 'no-cache'
  };
  if (token !== "") {
    headers = {
      ...headers,
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
      Accept: "application/json",
    };
  }
  return axios({
    method: 'get',
    url: 'http://mapi.comeup.asia/api/venues',
    // responseType: ''
  }).catch((e) => console.log("e", process.env.HOSTNAME));
}

export const Venues = () => {
  const query=""
  return useRefullAPI(query);
}
export const News = () => {
  const query = `
  query MyQuery {
    posts(first: 9) {
      nodes {
        date
        slug
        title
        content
        featuredImage {
          node {
            id
            mediaItemUrl
          }
        }
      }
    }
  }
    `;
  return useGrapql(query);
};

