import axios from 'axios';

// console.log('authh', JSON.parse(auth).data.token)

const useGrapql = (query, token = '', session = '') => {
  let headers = {}
  if (token !== '') {
    headers = {
      ...headers,
      "Authorization": `Bearer ${token}`,
      "Content-Type": "application/json",
      "Accept": "application/json",

    }
  }
  if (session !== '') {
    headers = {
      ...headers,
      'woocommerce-session': `Session ${session}`,
    }
  }



  return axios({
    method: 'POST',
    url: `${process.env.HOSTNAME_CMS}/graphql`,
    data: {
      query: query
    },
    headers: headers,
    timeout: 90000,

  }).catch(e => console.log('e', e))
}
// lấy danh sách sản phẩm
export const getProduct = (first = 8, categoryId = 0, after = '', search = '', slug = '') => {
  const query = `
  query MyQuery {
    products(first: ${first}, where: {status: "publish", categoryId: ${categoryId}, search:"${search}", slug: "${slug}"},after: "${after}") {
      nodes {
        id
        slug
        image {
          sourceUrl(size: THUMB_SIZE_PRODUCT)
        }
        name
        ... on SimpleProduct {
          price
          regularPrice
          id
          salePrice
          stockQuantity
        }
        ... on VariableProduct {
          price
          id
          regularPrice
          salePrice
          stockQuantity
          variations {
            nodes {
              price
              salePrice
              regularPrice
              stockQuantity
            }
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
    }
  }
  `
  return useGrapql(query)
}

// login
export const qrLogin = (username, password, clientMutationId = '') => {
  const query = `
  mutation MyMutation {
    login(input: {password: "${password}", username: "${username}", clientMutationId: "${clientMutationId}"}) {
      authToken
      clientMutationId
      customer {
        id
        databaseId
      }
      user {
        id
        userId
      }
    }
  }
  `
  return useGrapql(query)
}

// đăng ký tài khoản
export const qrRegister = (username, password, email) => {
  const query = `
  mutation MyMutation {
    registerUser(input: {username: "${email}", firstName: "${username}", password: "${password}", email: "${email}"}) {
      user {
        email
      }
    }
  }
  `
  return useGrapql(query)
}

// khởi tạo lại email
export const resendEmail = (email) => {
  const query = `
  mutation MyMutation {
    sendPasswordResetEmail(input: {username: "${email}"}) {
      user {
        email
      }
    }
  }
  `
  return useGrapql(query)
}

// kiểm tra giỏ hàng
export const checkCart = (id, idType = 'DATABASE_ID') => {
  const query = `
  query MyQuery {
    productVariation(id: "${id}", idType: ${idType}) {
      name
      price
      stockStatus
      parent {
        node {
          name
        }
      }
    }
  }
  `
  return useGrapql(query)
}

// lấy sản phẩm theo thể loại
export const getProductCategories = () => {
  const query = `
  query MyQuery {
    productCategories(first: 30) {
      nodes {
        name
        count
        slug
        parentDatabaseId
        databaseId
        menuOrder
      }
    }
  }
  `
  return useGrapql(query)
}

// lấy thông tin người dùng
export const getInforUser = (id, token) => {
  const query = `
  query MyQuery {
    customer(id: "${id}") {
      billing {
        address1
        address2
        email
        phone
        firstName
        city
      }
      id
      firstName
      email
    }
  }
  `
  return useGrapql(query, token)
}


// update thông tin người dùng
export const updateInfor = (idUser, idCustomer, firstName, phone, token) => {
  const query = `
  mutation MyMutation {
    updateUser(input: {id: "${idUser}", firstName: "${firstName}"}) {
      user {
        firstName
      }
    }
    updateCustomer(input: {id: "${idCustomer}", billing: {phone: "${phone}"}}) {
      customer {
        billing {
          phone
        }
      }
    }
  }
  `
  return useGrapql(query, token)
}

// update địa chỉ
export const updateAddress = (idCustomer, firstName, phone, city, address, token) => {
  const query = `
  mutation MyMutation {
    updateCustomer(input: {id: "${idCustomer}", billing: {phone: "${phone}", firstName: "${firstName}",city:"${city}",address1:"${address}"}}) {
      customer {
        billing {
          phone
        }
      }
    }
  }
  `
  return useGrapql(query, token)
}

// update password
export const updatePassword = (idUser, password, token) => {
  const query = `
  mutation MyMutation {
    updateUser(input: {id: "${idUser}", password: "${password}"}) {
      user {
        id
      }
    }
  }
  `
  return useGrapql(query, token)
}



// danh sách đơn hàng
export const getListOrder = (id, token) => {
  const query = `
  query MyQuery {
    orders(first:1000,where: {customerId: ${id}}) {
      nodes {
        date
        status
        total(format: FORMATTED)
        id
        databaseId
        billing {
          address1
          city
          email
          firstName
          phone
        }
        subtotal(format: FORMATTED)
        shippingLines {
          nodes {
            total
          }
        }
        lineItems {
          edges {
            node {
              quantity
              subtotal
              subtotalTax
              taxClass
              taxStatus
              total
              totalTax
              productId
              variation {
                name
              }
            }
          }
        }
      }
    }
  }
  `
  return useGrapql(query, token)
}
export const getFooter = () => {
  const query = `
  query MyQuery {
    footerVI {
      footer {
        address1
        address2
        fieldGroupName
        hotline
        mxh {
          facebook
          fieldGroupName
          youtube
          instagram
        }
      }
    }
    footerEN {
      footer {
        address1
        address2
        fieldGroupName
        hotline
        mxh {
          facebook
          fieldGroupName
          instagram
          youtube
        }
      }
    }
  }
  `
  return useGrapql(query)
}



export const sendOrders = (shipping, billing, lineItems, customerId = 0) => {
  const query = `
  mutation MyMutation {
    createOrder(input: {paymentMethod: "cod", paymentMethodTitle: "Cash on delivery",isPaid: false,
    shipping:{
      firstName:"fsfs",
      lastName: "",
      address1: "dffaf",
      address2: "",
      city: "HN",
      state: "",
      postcode: "90000",
      country: VN,
  },
  billing:{
    firstName:"fsfs",
    lastName: "",
    address1: "dffaf",
    address2: "",
    city: "HN",
    state: "",
    postcode: "90000",
    country: VN,
},
lineItems: {productId: 10, variationId: 10, quantity: 10},
shippingLines: {methodId: "flat_rate", methodTitle: "flat_rate", total: "0"}
}) {
      order {
        id
      }
    }
  }
  
  `
  return useGrapql(query)
}




export const addToCart = (productId, quanlity = 1, session) => {
  const query = `
  mutation MyMutation {
    addToCart(input: {productId: ${productId}, quantity: ${quanlity}}) {
      cartItem {
            key
            product {
              node {
                id
                productId: databaseId
                name
                description
                type
                onSale
                slug
                averageRating
                reviewCount
                image {
                  id
                  sourceUrl
                  altText
                }
                galleryImages {
                  nodes {
                    id
                    sourceUrl
                    altText
                  }
                }
              }
            }
            variation {
              node {
                id
                variationId: databaseId
                name
                description
                type
                onSale
                price
                regularPrice
                salePrice
                image {
                  id
                  sourceUrl
                  altText
                }
              }
              attributes {
                id
                attributeId
                name
                value
              }
            }
            quantity
            total
            subtotal
            subtotalTax
          }
    }
  }
  `
  return useGrapql(query, '', session)
}





export const getCart = (session) => {
  const query = `
  query GET_CART {
    cart {
      contents {
        nodes {
          product {
            node {
              id
              variationId: databaseId
              description
              type
              onSale
              slug
              averageRating
              reviewCount
            }
          }
          variation {
            node {
              id
              variationId: databaseId
              name
              description
              type
              onSale
              price
              regularPrice
              salePrice
              
            }
            attributes {
              id
              name
              value
            }
          }
          quantity
          total
          subtotal
          subtotalTax
        }
      }
      subtotal
      subtotalTax
      shippingTax
      shippingTotal
      total
      totalTax
      feeTax
      feeTotal
      discountTax
      discountTotal
    }
  }
  
  `
  return useGrapql(query, '', session)
}




export const clearCart = (session) => {
  const query = `
  mutation CLEAR_CART_MUTATION {
    removeItemsFromCart(input: {all: true}) {
      cartItems {
        quantity
      }
    }
  }
  `
  return useGrapql(query, '', session)
}



export const checkout = (input, token, session) => {
  const query = `
  mutation CHECKOUT_MUTATION {
    checkout(input: ${input}) {
      clientMutationId
      order {
        id
        orderKey
        orderNumber
        status
        refunds {
          nodes {
            amount
          }
        }
      }
      result
      redirect
    }
  }
  `
  return useGrapql(query, token, session)
}

export const getBanner = (input) => {
  const query = `
  query MyQuery {
    slidesTrangChu(first: 1) {
      nodes {
        slideTrangChu {
          slideHome {
            img {
              sourceUrl
            }
          }
        }
      }
    }
  }
  `
  return useGrapql(query)
}
export const getLogoClient = (input) => {
  const query = `
  query MyQuery {
    logoClient {
      clientLogo {
        logoClient {
          img {
            sourceUrl(size: MEDIUM)
          }
        }
      }
    }
  }  
  `
  return useGrapql(query)
}
export const getLogoHome = (input) => {
  const query = `
  query MyQuery {
    getHeader {
      siteLogoUrl
    }
  }
  `
  return useGrapql(query)
}
export const getDetailProduct = (slug) => {
  const query = `
  query MyQuery {
    products(where: {slug: "${slug}"}) {
      nodes {
        id
        databaseId
        slug
        description
        name
        ... on SimpleProduct {
          databaseId
          name
          price
          salePrice
          regularPrice
          stockStatus
          stockQuantity
        }
        ... on VariableProduct {
          databaseId
          name
          price
          salePrice
          regularPrice
          stockStatus
          stockQuantity
          variations {
            nodes {
              id
              databaseId
              stockStatus
              stockQuantity
              salePrice(format: FORMATTED)
              price
              regularPrice(format: FORMATTED)
              attributes {
                nodes {
                  label
                  value
                }
              }
              
            }
          }
        }
        attributes {
          nodes {
            name
            ... on GlobalProductAttribute {
              terms {
                nodes {
                  name
                }
              }
            }
          }
        }
        galleryImages {
          nodes {
            sourceUrl(size: LARGE)
          }
        }
        image {
          sourceUrl
        }
        related(first:4) {
          nodes {
            name
            ... on VariableProduct {
              id
              name
              slug
              price
              regularPrice
              salePrice
              stockStatus
              stockQuantity
              image{
                sourceUrl
              }
            }
          }
        }
      }
    }
  }
  `
  return useGrapql(query)
}






