import Image from 'next/image';
import React, { memo } from 'react';

const AblumImg = memo((props) => {
    console.log(props);
    return (
        <>
            <div className='bg-black inline-block w-full h-full pb-20'>
                <div className='container-outer mt-10 inline-block w-full h-full'>
                    <ul className='brandhub-thumb-list mx-auto w-full h-full'>
                        {
                            props.data.length > 0 && props.data.map((content,key) => {
                                return <>
                                    <li key={key} className={`brandhub-thumb-item ${key} ${key+1 === 1 ? 'brandhub-w-50' : ''}`}>
                                        <div className='embed-responsive embed-responsive-16by9'>
                                            <div className='embed-responsive-item'>
                                                <div className=' position-relative w-full h-full '>
                                                    <Image src={'https://www.mercedes-benz.com/en/vehicles/passenger-cars/inside-amg/_jcr_content/image/MQ6-8-image-20191022104423/00-mercedes-amg-inside-amg-series-2560x1440.jpeg'}
                                                        layout="fill"
                                                        className='brandhub-thumb-item-image'
                                                    />
                                                </div>
                                                <div className='brandhub-thumb-item-overlay brandhub-thumb-item-overlay-no-subtitle '>
                                                    <div className='brandhub-thumb-item-headline brandhub-parallax-affected'> INSIDE AMG. </div>
                                                    <div className='description brandhub-parallax-affected'>
                                                        <p>In our new video series, INSIDE AMG, Product Manager Felix Schönhofer meets talented Mercedes-AMG experts.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </>
                            })
                        }


                        {/* <li className='brandhub-thumb-item brandhub-w-50'>
                            <div className='embed-responsive embed-responsive-16by9'>
                                <div className='embed-responsive-item'>
                                    <Image src={'https://www.mercedes-benz.com/en/vehicles/passenger-cars/e-class/e-class-coupe-and-cabriolet/_jcr_content/image/MQ6-8-image-20200527000511/00-mercedes-benz-2020-e-class-coupe-cabriolet-2560x1440.jpeg'}
                                        layout="fill"
                                        className='brandhub-thumb-item-image'
                                    />
                                </div>
                            </div>
                        </li> */}
                    </ul>

                </div>
                <div className='w-full text-center mt-10'>
                    <button className='brandhub-thumb-more-button btn-dark-2 mx-auto text-center text-white'>
                        <brandhub-icon class="brandhub-icon brandhub-icon-arrow-down-inline" icon="chevron-down" width="12" height="14">
                            Show more
                        </brandhub-icon>
                    </button>
                </div>

            </div>
        </>
    );
});

export default AblumImg;