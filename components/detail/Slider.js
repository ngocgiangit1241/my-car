import React, { memo } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from "swiper";
// Import Swiper styles
import "swiper/css/pagination";
import Image from 'next/image';

const Slider = memo(() => {
    return (
        <><div className='h-[520px]'>
            <Swiper
               slidesPerView={"auto"}
                centeredSlides={true}
                spaceBetween={30}
                grabCursor={true}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination]}
                className="mySwiper"
            >
                {/* https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/mediagallery/parsys/mediagalleryitem_1853827168/mobileImage/MQ2-0-mobileImage-20191106134222/mercedes-benz-eqc-n293-brandhub-media-gallery-exterior-mobile-02-630x630-11-2019.jpeg */}
                <SwiperSlide>
                    <Image src='https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/mediagallery/parsys/mediagalleryitem_1853827168/image/MQ6-0-image-20191106134222/mercedes-benz-eqc-n293-brandhub-media-gallery-exterior-02-1280x720-11-2019.jpeg' 
                    layout="fill"
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <Image src='https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/mediagallery/parsys/mediagalleryitem_1853827168/image/MQ6-0-image-20191106134222/mercedes-benz-eqc-n293-brandhub-media-gallery-exterior-02-1280x720-11-2019.jpeg' 
                    layout="fill"
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <Image src='https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/mediagallery/parsys/mediagalleryitem_1853827168/image/MQ6-0-image-20191106134222/mercedes-benz-eqc-n293-brandhub-media-gallery-exterior-02-1280x720-11-2019.jpeg' 
                    layout="fill"
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <Image src='https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/mediagallery/parsys/mediagalleryitem_1853827168/image/MQ6-0-image-20191106134222/mercedes-benz-eqc-n293-brandhub-media-gallery-exterior-02-1280x720-11-2019.jpeg' 
                    layout="fill"
                    />
                </SwiperSlide>
            </Swiper>
        </div>
        </>
    );
});

export default Slider;