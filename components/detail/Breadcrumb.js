import Link from 'next/link';
import React, { memo } from 'react';

const Breadcrumb = memo((props) => {
    return (
        <>
            <div className='breadcrumb'>
                <div className='py-6'>
                    <div className='container px-16'>
                        <nav>
                            <ul className='flex text-lg'>
                                <li className='breadcrumb__item flex'>
                                    <Link href={'#'}>
                                        <a className='breadcrumb__link flex'>
                                            Home
                                            <svg className="breadcrumb__icon h-3 ml-2.5 w-1.5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 33.5">
                                                <path fill="#333" d="M0 0h5.163l14.829 16.81L5.343 33.5H.12l12.848-16.69z"></path>
                                            </svg>
                                        </a>
                                    </Link>
                                </li>
                                <li className='breadcrumb__item flex'>
                                    <Link href={'#'}>
                                        <a className='breadcrumb__link flex'>
                                            Home
                                            <svg className="breadcrumb__icon h-3 ml-2.5 w-1.5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 33.5">
                                                <path fill="#333" d="M0 0h5.163l14.829 16.81L5.343 33.5H.12l12.848-16.69z"></path>
                                            </svg>
                                        </a>
                                    </Link>
                                </li>
                                <li className='breadcrumb__item flex'>
                                    <Link href={'#'}>
                                        <a className='breadcrumb__link flex'>
                                            Home
                                            <svg className="breadcrumb__icon h-3 ml-2.5 w-1.5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 33.5">
                                                <path fill="#333" d="M0 0h5.163l14.829 16.81L5.343 33.5H.12l12.848-16.69z"></path>
                                            </svg>
                                        </a>
                                    </Link>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>

        </>
    );
});

export default Breadcrumb;