import Image from 'next/image';
import Link from 'next/link';
import React, { memo } from 'react';

const Banner = memo(() => {
    return (
        <>
            {/* loop */}
            <div className='relative brandhub-full-screen-stage--has-header has_video'>
                <div className='brandhub-full-screen-stage__media-container z-10'>
                    <video className='md:block hidden brandhub-full-screen-stage__preview-video' id="background-video" muted autoPlay>
                        <source src={'https://www.mercedes-benz.com/content/dam/brandhub/vehicles/passenger-cars/eqc/the-eqc-campaign/web/slide1_top_stage/11-2019/EQC_stage_video.mp4'} type="video/mp4" />
                    </video>
                    <video className='md:hidden block brandhub-full-screen-stage__preview-video' id="background-video" muted autoPlay>
                        <source src={'https://www.mercedes-benz.com/content/dam/brandhub/vehicles/passenger-cars/eqc/the-eqc-campaign/web/slide1_top_stage/11-2019/EQC_Final_Mobile_FHD_Uncompressed.mp4'} type="video/mp4" />
                    </video>
                    <Image src={'https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/fullscreenstage/image/MQ7-0-image-20210714110811/stage_eqc.jpeg'}
                        layout="fill"
                        className='img-responsive hidden' />
                </div>
                <div className='w-full h-full relative md:giang z-20 tracking-tight'>
                    <div className='h-full flex flex-col justify-between text-white container px-8 md:px-4 mx-auto'>
                        <div className=' max-w-[19rem] md:max-w-[24rem] my-[5vh]'>
                            <div className=' md:text-4xl mb-4 font-new text-6xl'>Enjoy Electric</div>
                            <div>Mercedes-Benz EQC: The Mercedes-Benz all-new EQC 400 4MATIC, provides impressive range, fast charging, and the thrilling feeling that only driving a Mercedes-Benz brings.</div>
                        </div>
                        <div >
                            <ul className='flex justify-between'>
                                <li className='md:flex flex-col justify-end animation hidden mr-7'>
                                    <span className='mb-2 text-sm md:text-lg'>
                                        Range
                                    </span>
                                    <span className=' md:text-3xl lg:text-5xl font-new font-thin whitespace-nowrap'>
                                        429,0-454,0 km⁶
                                    </span>
                                </li>
                                <li className='md:flex flex-col justify-end animation hidden mr-7'>
                                    <span className='mb-2 text-sm md:text-lg'>
                                        Fully variable 4-wheel drive
                                    </span>
                                    <span className=' md:text-3xl lg:text-5xl font-new font-thin whitespace-nowrap'>
                                        4MATIC
                                    </span>
                                </li>
                                <li className='md:flex flex-col justify-end animation hidden mr-7'>
                                    <span className='mb-2 text-sm md:text-lg'>
                                        0-100 km/h in just
                                    </span>
                                    <span className=' md:text-3xl  lg:text-5xl font-new font-thin whitespace-nowrap'>
                                        5,1 Sec
                                    </span>
                                </li>
                                <li className='flex flex-col justify-end max-w-sm animation'>
                                    <p>EQC 400 4MATIC:
                                        Stromverbrauch kombiniert: 21,5–20,1 kWh/100 km;
                                        CO₂-Emissionen kombiniert: 0 g/km.⁶
                                    </p>

                                </li>
                            </ul>
                            <div className=' h-28 py-3 w-full mx-auto text-[hsla(0,0%,100%,.5)] border-t border- border-[hsla(0,0%,100%,0.2)]'>
                                <ul className='hidden md:flex h-full justify-center items-center text-lg'>
                                    <li className='flex text-white'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>EQC</a>
                                        </Link>
                                    </li>
                                    <li className='flex'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>Story</a>
                                        </Link>
                                    </li>
                                    <li className='flex'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>Exterior</a>
                                        </Link>
                                    </li>
                                    <li className='flex'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>Interior</a>
                                        </Link>
                                    </li>
                                    <li className='flex'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>Charging</a>
                                        </Link>
                                    </li>
                                    <li className='flex'>
                                        <Link href={'#EQC'}>
                                            <a className='px-4'>Connectivity</a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
});

export default Banner;