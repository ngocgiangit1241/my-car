import Image from 'next/image';
import React, { memo } from 'react';

const SliderInfo = memo(() => {
    return (
        <>
            <div>
                <div className='bg-black w-full text-white py-12'>
                    <div className=' max-w-[85%] px-4 mx-auto'>
                        <div className='w-1/2'>
                            <h2 className=' text-5xl font-light'>Get in. Get in touch.
                                The MBUX intuitive system.</h2>
                            <div className='font-light text-[#999] my-4 text-lg'>
                                <p>
                                    Explore the MBUX interior assistant, the revolutionary infotainment system: with natural voice control, touch-sensitive surfaces, and Augmented Reality for navigation.
                                </p>
                            </div>

                        </div>

                    </div>
                    <div className=''>
                            <Image src={'https://www.mercedes-benz.com/en/vehicles/passenger-cars/eqc/_jcr_content/root/tabgallery/tabitems/tabgalleryitem/image/MQ6-0-image-20191106140146/mercedes-benz-eqc-n293-brandhub-tab-gallery-interior-2000X875-11-2019.jpeg'}
                                layout="responsive" width={700}
                                height={300}/>
                    </div>
                </div>
            </div>

        </>
    );
});

export default SliderInfo;