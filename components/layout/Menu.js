import Image from 'next/image';
import Link from 'next/link';
import React, { memo } from 'react';

export const Menu = memo((props) => {
    return (
        <div>
            <div className=' h-20'>

            </div>
            <div className=' fixed top-0 w-full bg-black z-[9999]'>
                <div className=' container px-4 mx-auto'>
                    <div className='w-full flex items-center h-20 justify-between'>
                        <Link href={'#'}>
                            <a className='flex items-center'>
                                <div className='mr-4'>
                                    <Image src={'https://www.mercedes-benz.com/etc/designs/brandhub/frontend/static-assets/header/logo.svg'}
                                        alt="Picture of the author"
                                        layout="intrinsic"
                                        className='w-full '
                                        width={64}
                                        height={64}
                                    />
                                </div>
                                <div className=' -mt-2'>
                                    <Image src={'https://www.mercedes-benz.com/etc/designs/brandhub/frontend/static-assets/header/brand-without-claim.svg'}
                                        alt="Picture of the author"
                                        layout="intrinsic"
                                        className='w-20'
                                        width={120}
                                        height={32}
                                    />
                                </div>
                            </a>
                        </Link>
                        <ul className='hidden text-white md:flex justify-around items-center font-light'>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Mẫu xe
                                    </a>
                                </Link>
                            </li>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Thiết kế
                                    </a>
                                </Link>
                            </li>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Sự đổi mới
                                    </a>
                                </Link>
                            </li>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Bảo tàng & Lịch sử
                                    </a>
                                </Link>
                            </li>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Sự kiện
                                    </a>
                                </Link>
                            </li>
                            <li className='px-5'>
                                <Link href={'#'} >
                                    <a>
                                        Cách sống
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>

    );
});
