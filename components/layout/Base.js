import React, { memo } from 'react';
import { Menu } from './Menu';

const Base = memo((props) => {
    return (
        <>

            <div className={props.children.type?.name}>
                <div>
                    <Menu />
                </div>
                {props.children}
            </div>
        </>
    );
});

export default Base;