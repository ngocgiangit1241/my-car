

import React, { memo, useState } from 'react';

const TreeView = memo((props) => {
    console.log(props);
    // function TreeView({
    //     data,
    //     toggled = true,
    //     name = null,
    //     isLast = true,
    //     isChildElement = false,
    //     isParentToggled = true
    // })
    const [data, setdata] = useState(props.data);
    const [isToggled, setIsToggled] = useState(props.toggled ?? true);
    const [name, setName] = useState(props.name ?? null);
    const [isLast, setIsLast] = useState(props.isLast ?? null);
    const [isChildElement, setIsChildElement] = useState(props.isChildElement ?? null);
    const [isParentToggled, setIsParentToggled] = useState(props.isParentToggled ?? null);


    return (
        <div
            style={{ marginLeft: isChildElement ? 16 : 4 + "px" }}
            className={isParentToggled ? "tree-element" : "tree-element collapsed"}
        >
            <span
                className={isToggled ? "toggler" : "toggler closed"}
                onClick={() => setIsToggled(!isToggled)}
            />
            {name ? <strong>&nbsp;&nbsp;{name}: </strong> : <span>&nbsp;&nbsp;</span>}
            {Array.isArray(data) ? `[` : `{`}
            {!isToggled && `...`}
            {Object.keys(data).map(
                function (v, i, a) {
                    return typeof data[v] == "object" ? (
                        <TreeView
                            key={i}
                            data={data[v]}
                            isLast={i === a.length - 1}
                            name={Array.isArray(data) ? null : v}
                            isChildElement
                            isParentToggled={isParentToggled && isToggled}
                        />
                    ) : (
                        <p key={i}
                            style={{ marginLeft: 16 + "px" }}
                            className={isToggled ? "tree-element" : "tree-element collapsed"}
                        >
                            {Array.isArray(data) ? "" : <strong>{v}: </strong>}
                            {data[v]}
                            {i === a.length - 1 ? "" : ","}
                        </p>
                    )
                })
            }

            {Array.isArray(data) ? `]` : `}`}
            {!isLast ? `,` : ""}
        </div>
    );
});

export default TreeView;