import React, { memo } from 'react';
import TreeView from '../components/TreeView';

const test = memo(() => {
    let data = {
        lorem: {
          ipsum: "dolor sit",
          amet: {
            consectetur: "adipiscing",
            elit: [
              "duis",
              "vitae",
              {
                semper: "orci"
              },
              {
                est: "sed ornare"
              },
              "etiam",
              ["laoreet", "tincidunt"],
              ["vestibulum", "ante"]
            ]
          },
          ipsum: "primis"
        }
      };
    return (
        <>
            <TreeView data={data} name={'giang'}/>
        </>
    );
});

export default test;