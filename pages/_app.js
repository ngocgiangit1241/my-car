import '../styles/globals.css'
import Base from '../components/layout/Base'

function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout ? Component.Layout : Base;
  return (
    <>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}

export default MyApp
